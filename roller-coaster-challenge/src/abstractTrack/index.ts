import {
  createCurve,
  createEnd,
  createStart,
  createStraight,
} from '../manipulations/create';
import {
  rotateDirection,
} from '../manipulations/rotate';
import {
  Direction,
  Track,
} from '../types';

function travel([x, y]: [x: number, y: number], direction: Direction, length: number): [number, number] {
  length++;
  if (direction === Direction.NORTH) return [x, y - length];
  if (direction === Direction.EAST) return [x + length, y];
  if (direction === Direction.SOUTH) return [x, y + length];
  if (direction === Direction.WEST) return [x - length, y];
  throw Error('Invalid direction');
}

function travelCurve(direction: Direction, turn: 'r'|'l'): Direction {
  if (turn === 'r') return rotateDirection(direction, 1);
  if (turn === 'l') return rotateDirection(direction, -1);
  throw Error('Invalid turn');
}

export function determineDimensions(abstractTrack, startDirection: Direction = Direction.NORTH) {
  let minX = 0, minY = 0, maxX = 0, maxY = 0;
  let x = 0;
  let y = 0;
  let direction = startDirection;
  for (let i = 0; i < abstractTrack.length; i++) {
    if (i % 2 === 0) { // straight track
      const { length } = abstractTrack[i];
      [x, y] = travel([x, y], direction, length);
      if (x < minX) minX = x;
      if (x > maxX) maxX = x;
      if (y < minY) minY = y;
      if (y > maxY) maxY = y;
    } else { // curved track
      const turn = abstractTrack[i];
      direction = travelCurve(direction, turn);
    }
  }
  return {
    minX,
    minY,
    maxX,
    maxY,
  };
};

export function determineHeight(abstractTrack): number {
  let totalHeight = 1;
  for (let i = 0; i < abstractTrack.length; i += 2) {
    totalHeight += abstractTrack[i].drop;
  }
  return totalHeight;
};

export function generateTrack(abstractTrack, startDirection: Direction = Direction.NORTH) {
  const { minX, minY, maxX, maxY } = determineDimensions(abstractTrack);
  const track: Track = {
    xLength: maxX - minX,
    yLength: maxY - minY,
    posts: [],
    trackPieces: [],
  };
  let x = 0;
  let y = 0;
  let height = determineHeight(abstractTrack);
  let direction = startDirection;
  track.trackPieces.push(createStart({
    x: 0,
    y: 0,
    height,
    direction,
  }));
  for (let i = 0; i < abstractTrack.length; i++) {
    if (i % 2 === 0) { // straight track
      const {length, drop} = abstractTrack[i];
      track.trackPieces.push(createStraight({
        x,
        y,
        height,
        direction,
        length,
        drop,
      }));
      [x, y] = travel([x, y], direction, length);
      height -= drop;
    } else { // curved track
      const turn = abstractTrack[i];
      track.trackPieces.push(createCurve({
        x,
        y,
        height,
        direction,
        turn: turn === 'r' ? 'right' : 'left',
      }));
      direction = travelCurve(direction, turn);
    }
  }
  track.trackPieces.push(createEnd({
    x,
    y,
    height,
    direction,
  }));
  return track;
}
