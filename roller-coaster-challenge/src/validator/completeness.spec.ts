import {
    createStart,
    createTrack,
} from '../manipulations/create';
import {
    arePositionsEqualGenerator,
    findPieceAt,
} from './completeness';

describe('arePositionsEqualGenerator', () => {
    test('returns true when the positions are equal', () => {
        const a = {x: 2, y: 3, height: 4};
        const b = {x: 2, y: 3, height: 4};
        const func = arePositionsEqualGenerator(a);
        expect(func(b)).toBe(true);
    });
    test('returns false when x differs', () => {
        const a = {x: 2, y: 3, height: 4};
        const b = {x: 1, y: 3, height: 4};
        const func = arePositionsEqualGenerator(a);
        expect(func(b)).toBe(false);
    });
    test('returns false when y differs', () => {
        const a = {x: 2, y: 3, height: 4};
        const b = {x: 2, y: 1, height: 4};
        const func = arePositionsEqualGenerator(a);
        expect(func(b)).toBe(false);
    });
    test('returns false when height differs', () => {
        const a = {x: 2, y: 3, height: 4};
        const b = {x: 2, y: 3, height: 1};
        const func = arePositionsEqualGenerator(a);
        expect(func(b)).toBe(false);
    });
});

describe('findPieceAt', () => {
    test('returns undefined when no piece is at the position', () => {
        const track = createTrack();
        const position = {x: 2, y: 3, height: 4};
        expect(findPieceAt(track, position)).toBeUndefined();
    });
    test('finds the piece', () => {
        const track = createTrack();
        const position = {x: 2, y: 3, height: 4};
        const piece = createStart(position);
        track.trackPieces.push(piece);
        expect(findPieceAt(track, position)).toBe(piece);
    });
});
