import { Track } from "../types";

export function getAccumulatedPostHeight({posts}: Track): number {
    return posts.reduce((height, post) => (height + post.height), 0);
}

export function validatePostPositions(track) {
    track.posts.forEach((post) => {
        if (post.x < 0 || post.y < 0 || post.x >= track.xLength || post.y >= track.yLength) {
            throw new Error(`Post outside the board: (${post.x}|${post.y})`);
        }
    });
    track.posts.forEach((post1) => {
        track.posts.forEach((post2) => {
            if (post1 !== post2 && post1.x === post2.x && post1.y === post2.y) {
                throw new Error(`Posts overlapping: (${post1.x}|${post1.y})`);
            }
        });
    });
    return true;
}
