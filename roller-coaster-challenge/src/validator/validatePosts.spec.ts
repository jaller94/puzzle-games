import {
    getAccumulatedPostHeight,
    validatePostPositions,
} from './validatePosts';

describe('getAccumulatedPostHeight', () => {
    test('passes on 0 posts', () => {
        const track = {
            xLength: 5,
            yLength: 5,
            posts: [],
        } as any;
        expect(getAccumulatedPostHeight(track)).toBe(0);
    });
    test('passes on 1 post', () => {
        const track = {
            xLength: 5,
            yLength: 5,
            posts: [
                { x: 0, y: 0, height: 1 },
            ],
        } as any;
        expect(getAccumulatedPostHeight(track)).toBe(1);
    });
    test('passes on 2 post', () => {
        const track = {
            xLength: 5,
            yLength: 5,
            posts: [
                { x: 0, y: 0, height: 3 },
                { x: 0, y: 0, height: 4 },
            ],
        } as any;
        expect(getAccumulatedPostHeight(track)).toBe(7);
    });
});

describe('validatePosts', () => {
    test('passes on valid posts', () => {
        const track = {
            xLength: 5,
            height: 7,
            posts: [
                { x: 0, y: 0, height: 1 },
                { x: 4, y: 0, height: 1 },
                { x: 4, y: 6, height: 1 },
                { x: 0, y: 6, height: 1 },
            ],
        };
        expect(validatePostPositions(track)).toBe(true);
    });
    test('throws error when a post is too north', () => {
        const track = {
            xLength: 5,
            yLength: 5,
            posts: [
                { x: 0, y: -1, height: 1 },
            ],
        };
        const func = () => {validatePostPositions(track)};
        expect(func).toThrow('Post outside the board: (0|-1)');
    });
    test('throws error when a post is too east', () => {
        const track = {
            xLength: 5,
            height: 1,
            posts: [
                { x: 5, y: 0, height: 1 },
            ],
        };
        const func = () => {validatePostPositions(track)};
        expect(func).toThrow('Post outside the board: (5|0)');
    });
    test('throws error when a post is too south', () => {
        const track = {
            xLength: 1,
            yLength: 5,
            posts: [
                { x: 0, y: 5, height: 1 },
            ],
        };
        const func = () => {validatePostPositions(track)};
        expect(func).toThrow('Post outside the board: (0|5)');
    });
    test('throws error when a post is too west', () => {
        const track = {
            xLength: 5,
            yLength: 5,
            posts: [
                { x: -1, y: 0, height: 1 },
            ],
        };
        const func = () => {validatePostPositions(track)};
        expect(func).toThrow('Post outside the board: (-1|0)');
    });
});
