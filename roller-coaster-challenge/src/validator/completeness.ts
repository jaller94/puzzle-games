import { Position, Track } from "../types";

export const arePositionsEqualGenerator = (pos1: Position) => (pos2: Position) => (
    pos1.x === pos2.x && pos1.y === pos2.y && pos1.height === pos2.height
);

export function findPieceAt({trackPieces}: Track, position: Position) {
    const isCorrect = arePositionsEqualGenerator(position);
    return (trackPieces || []).find(isCorrect);
}

export function validateTrackCompleteness(track: Track) {
    throw Error('NOT IMPLEMENTED');
}
