import * as fs from 'node:fs';

import {
    inventory,
} from './fixtures/inventory';
import {
    generateValidDefaultTrack,
} from './generate';

const track = generateValidDefaultTrack(inventory);
fs.writeFileSync('track.json', JSON.stringify(track, null, 2), 'utf8');
