import {
    Track,
    TrackPiece,
} from '../types';

export const createCurve = (params): TrackPiece => ({
    type: 'curve',
    x: 0,
    y: 0,
    height: 1,
    direction: 'N',
    turn: 'right',
    ...params,
});

export const createEnd = (params): TrackPiece => ({
    type: 'end',
    x: 0,
    y: 0,
    height: 1,
    direction: 'N',
    ...params,
});

export const createStart = (params): TrackPiece => ({
    type: 'start',
    x: 0,
    y: 0,
    height: 1,
    direction: 'N',
    ...params,
});

export const createStraight = (params): TrackPiece => ({
    type: 'straight',
    x: 0,
    y: 0,
    height: 1,
    direction: 'N',
    length: 1,
    drop: 0,
    ...params,
});

export const createTrack = (params?: Record<string, unknown>): Track => ({
    xLength: 5,
    yLength: 5,
    posts: [],
    trackPieces: [],
    tunnels: [],
    ...params,
});
