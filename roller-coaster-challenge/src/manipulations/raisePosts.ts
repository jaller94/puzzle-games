import {
    getCurves,
    getEnd,
    getStart,
} from '../queries/filters';
import {
    rotateDirection,
} from './rotate';
import {
    Direction,
    Track,
} from '../types';

function getPostPositionOfCurvedTrack({x, y, direction, turn}): {x: number, y: number} {
    if (!['right', 'left'].includes(turn)) throw new Error(`Unknown turn: ${turn}`);
    if (!'NESW'.includes(direction)) throw new Error(`Unknown direction: ${direction}`);
    if (turn === 'right') {
        if (direction === Direction.NORTH) return { x: x, y };
        if (direction === Direction.EAST) return { x: x - 1 , y };
        if (direction === Direction.SOUTH) return { x: x - 1, y: y - 1 };
        if (direction === Direction.WEST) return { x, y: y - 1 };
    } else if (turn === 'left') {
        if (direction === Direction.NORTH) return { x: x - 1, y };
        if (direction === Direction.EAST) return { x: x - 1, y: y - 1 };
        if (direction === Direction.SOUTH) return { x, y: y - 1 };
        if (direction === Direction.WEST) return { x, y };
    }
    throw Error('Invalid input');
}

export function raisePost(track: Track, x: number, y: number, height: number): Track {
    track.posts = track.posts || [];
    const post = track.posts.find((post) => (
        post.x === x && post.y === y
    ));
    if (post && post.height < height) {
        post.height = height;
    }
    if (!post) {
        track.posts.push({ x, y, height });
    }
    return track;
}

function raisePostsForStart(track: Track, toTheLeft = false): Track {
    const start = getStart(track.trackPieces);
    if (!start) return track;
    if (start.x === 0 && start.direction === Direction.SOUTH) {
        toTheLeft = true;
    }
    if (start.y === 0 && start.direction === Direction.WEST) {
        toTheLeft = true;
    }
    if (start.x === track.xLength && start.direction === Direction.NORTH) {
        toTheLeft = true;
    }
    if (start.y === track.yLength && start.direction === Direction.EAST) {
        toTheLeft = true;
    }
    const {x, y} = getPostPositionOfCurvedTrack({
        x: start.x,
        y: start.y,
        direction: rotateDirection(start.direction, toTheLeft ? 1 : 2),
        turn: 'left',
    });
    return raisePost(track, x, y, start.height);
}

function raisePostsForEnd(track: Track, toTheLeft = false): Track {
    const end = getEnd(track.trackPieces);
    if (!end) return track;
    if (end.x === 0 && end.direction === Direction.SOUTH) {
        toTheLeft = true;
    }
    if (end.y === 0 && end.direction === Direction.WEST) {
        toTheLeft = true;
    }
    if (end.x === track.xLength && end.direction === Direction.NORTH) {
        toTheLeft = true;
    }
    if (end.y === track.yLength && end.direction === Direction.EAST) {
        toTheLeft = true;
    }
    const {x, y} = getPostPositionOfCurvedTrack({
        x: end.x,
        y: end.y,
        direction: end.direction,
        turn: toTheLeft ? 'left' : 'right',
    });
    return raisePost(track, x, y, end.height);
}

export function raisePosts(track: Track, leftOfStart?: boolean, leftOfEnd?: boolean): Track {
    let resultTrack = track;
    getCurves(track.trackPieces).forEach((curvedTrack) => {
        const {x, y} = getPostPositionOfCurvedTrack(curvedTrack);
        resultTrack = raisePost(resultTrack, x, y, curvedTrack.height);
    });
    if (getStart(track.trackPieces)) {
        resultTrack = raisePostsForStart(resultTrack, leftOfStart);
    }
    if (getEnd(track.trackPieces)) {
        resultTrack = raisePostsForEnd(resultTrack, leftOfEnd);
    }
    return resultTrack;
}
