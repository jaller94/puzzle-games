import {
    raisePost,
    raisePosts,
} from './raisePosts';
import {
    createCurve,
    createEnd,
    createStart,
    createTrack,
} from './create';

describe('raisePost', () => {
    test('raises a new post', () => {
        const track = createTrack();
        const actual = raisePost(track, 0, 0, 1);
        expect(actual.posts).toEqual([
            { x: 0, y: 0, height: 1 },
        ]);
    });
    test('increases height of a smaller post', () => {
        const track = createTrack({
            posts: [
                { x: 0, y: 0, height: 1 },
            ],
        });
        const actual = raisePost(track, 0, 0, 5);
        expect(actual.posts).toEqual([
            { x: 0, y: 0, height: 5 },
        ]);
    });
    test('keeps height of a correct post', () => {
        const track = createTrack({
            posts: [
                { x: 0, y: 1, height: 4 },
            ],
        });
        const actual = raisePost(track, 0, 1, 4);
        expect(actual.posts).toEqual([
            { x: 0, y: 1, height: 4 },
        ]);
    });
    test('keeps height of a taller post', () => {
        const track = createTrack({
            posts: [
                { x: 1, y: 0, height: 11 },
            ],
        });
        const actual = raisePost(track, 1, 0, 9);
        expect(actual.posts).toEqual([
            { x: 1, y: 0, height: 11 },
        ]);
    });
});

describe('raisesPosts', () => {
    describe('raises correct post for a right curve', () => {
        test('oriented North', () => {
            const track = createTrack();
            const curve = createCurve({
                direction: 'N',
                turn: 'right',
            });
            track.trackPieces.push(curve);
            const actual = raisePosts(track);
            expect(actual.posts).toEqual([
                { x: 0, y: 0, height: curve.height },
            ]);
        });
        test('oriented East', () => {
            const track = createTrack();
            const curve = createCurve({
                direction: 'E',
                turn: 'right',
            });
            track.trackPieces.push(curve);
            const actual = raisePosts(track);
            expect(actual.posts).toEqual([
                { x: -1, y: 0, height: curve.height },
            ]);
        });
        test('oriented South', () => {
            const track = createTrack();
            const curve = createCurve({
                direction: 'S',
                turn: 'right',
            });
            track.trackPieces.push(curve);
            const actual = raisePosts(track);
            expect(actual.posts).toEqual([
                { x: -1, y: -1, height: curve.height },
            ]);
        });
        test('oriented West', () => {
            const track = createTrack();
            const curve = createCurve({
                direction: 'W',
                turn: 'right',
            });
            track.trackPieces.push(curve);
            const actual = raisePosts(track);
            expect(actual.posts).toEqual([
                { x: 0, y: -1, height: curve.height },
            ]);
        });
    });
    describe('raises correct post for a left curve', () => {
        test('oriented North', () => {
            const track = createTrack();
            const curve = createCurve({
                direction: 'N',
                turn: 'left',
            });
            track.trackPieces.push(curve);
            const actual = raisePosts(track);
            expect(actual.posts).toEqual([
                { x: -1, y: 0, height: curve.height },
            ]);
        });
    });

    describe('raises correct post for the start', () => {
        test('on the left to the North', () => {
            const track = createTrack();
            const start = createStart({
                direction: 'N',
                x: 2,
                y: 2,
            });
            track.trackPieces.push(start);
            const actual = raisePosts(track, true);
            expect(actual.posts).toEqual([
                { x: start.x - 1, y: start.y - 1, height: start.height },
            ]);
        });
        test('on the left to the East', () => {
            const track = createTrack();
            const start = createStart({
                direction: 'E',
                x: 2,
                y: 2,
            });
            track.trackPieces.push(start);
            const actual = raisePosts(track, true);
            expect(actual.posts).toEqual([
                { x: start.x, y: start.y - 1, height: start.height },
            ]);
        });
        test('on the right to the North', () => {
            const track = createTrack();
            const start = createStart({
                direction: 'N',
                x: 2,
                y: 2,
            });
            track.trackPieces.push(start);
            const actual = raisePosts(track, false);
            expect(actual.posts).toEqual([
                { x: start.x, y: start.y - 1, height: start.height },
            ]);
        });
        test('on the right to the East', () => {
            const track = createTrack();
            const start = createStart({
                direction: 'E',
                x: 2,
                y: 2,
            });
            track.trackPieces.push(start);
            const actual = raisePosts(track, false);
            expect(actual.posts).toEqual([
                { x: start.x, y: start.y, height: start.height },
            ]);
        });
    });
    describe('raises correct post for the end', () => {
        test('on the right to the North', () => {
            const track = createTrack();
            const end = createEnd({
                direction: 'N',
                x: 2,
                y: 2,
            });
            track.trackPieces.push(end);
            const actual = raisePosts(track, undefined, false);
            expect(actual.posts).toEqual([
                { x: end.x, y: end.y, height: end.height },
            ]);
        });
        test('on the right to the East', () => {
            const track = createTrack();
            const end = createEnd({
                direction: 'E',
                x: 2,
                y: 2,
            });
            track.trackPieces.push(end);
            const actual = raisePosts(track, undefined, false);
            expect(actual.posts).toEqual([
                { x: end.x - 1, y: end.y, height: end.height },
            ]);
        });
        test('on the right to the South', () => {
            const track = createTrack();
            const end = createEnd({
                direction: 'S',
                x: 2,
                y: 2,
            });
            track.trackPieces.push(end);
            const actual = raisePosts(track, undefined, false);
            expect(actual.posts).toEqual([
                { x: end.x - 1, y: end.y - 1, height: end.height },
            ]);
        });
        test('on the right to the West', () => {
            const track = createTrack();
            const end = createEnd({
                direction: 'W',
                x: 2,
                y: 2,
            });
            track.trackPieces.push(end);
            const actual = raisePosts(track, undefined, false);
            expect(actual.posts).toEqual([
                { x: end.x, y: end.y - 1, height: end.height },
            ]);
        });
        test('on the left from the North', () => {
            const track = createTrack();
            const end = createEnd({
                direction: 'N',
                x: 2,
                y: 2,
            });
            track.trackPieces.push(end);
            const actual = raisePosts(track, undefined, true);
            expect(actual.posts).toEqual([
                { x: end.x - 1, y: end.y, height: end.height },
            ]);
        });
        test('on the left from the East', () => {
            const track = createTrack();
            const end = createEnd({
                direction: 'E',
                x: 2,
                y: 2,
            });
            track.trackPieces.push(end);
            const actual = raisePosts(track, undefined, true);
            expect(actual.posts).toEqual([
                { x: end.x - 1, y: end.y - 1, height: end.height },
            ]);
        });
        test('on the left from the South', () => {
            const track = createTrack();
            const end = createEnd({
                direction: 'S',
                x: 2,
                y: 2,
            });
            track.trackPieces.push(end);
            const actual = raisePosts(track, undefined, true);
            expect(actual.posts).toEqual([
                { x: end.x, y: end.y - 1, height: end.height },
            ]);
        });
        test('on the left from the West', () => {
            const track = createTrack();
            const end = createEnd({
                direction: 'W',
                x: 2,
                y: 2,
            });
            track.trackPieces.push(end);
            const actual = raisePosts(track, undefined, true);
            expect(actual.posts).toEqual([
                { x: end.x, y: end.y, height: end.height },
            ]);
        });
    });
});