import { Direction } from "../types";

/**
 * Returns how a direction changes after 90° rotations.
 */
export function rotateDirection(direction: Direction, rotations: number): Direction {
    const directions = [Direction.NORTH, Direction.EAST, Direction.SOUTH, Direction.WEST];
    const index = directions.indexOf(direction);
    const newIndex = (((index + rotations) % 4 ) + 4 ) % 4;
    return directions[newIndex];
}
