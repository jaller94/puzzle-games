import { Direction } from '../types';
import {
    rotateDirection,
} from './rotate';

describe('rotateDirection', () => {
    test('1 rotation from N is E', () => {
        expect(rotateDirection(Direction.NORTH, 1)).toBe(Direction.EAST);
    });
    test('2 rotations from N is S', () => {
        expect(rotateDirection(Direction.NORTH, 2)).toBe(Direction.SOUTH);
    });
    test('3 rotations from N is W', () => {
        expect(rotateDirection(Direction.NORTH, 3)).toBe(Direction.WEST);
    });
    test('4 rotations from N is N', () => {
        expect(rotateDirection(Direction.NORTH, 4)).toBe(Direction.NORTH);
    });
    test('17 rotations from N is E', () => {
        expect(rotateDirection(Direction.NORTH, 17)).toBe(Direction.EAST);
    });
    test('-1 rotations from N is W', () => {
        expect(rotateDirection(Direction.NORTH, -1)).toBe(Direction.WEST);
    });
    test('1 rotation from E is S', () => {
        expect(rotateDirection(Direction.EAST, 1)).toBe(Direction.SOUTH);
    });
    test('1 rotation from S is W', () => {
        expect(rotateDirection(Direction.SOUTH, 1)).toBe(Direction.WEST);
    });
    test('1 rotation from W is N', () => {
        expect(rotateDirection(Direction.WEST, 1)).toBe(Direction.NORTH);
    });
});
