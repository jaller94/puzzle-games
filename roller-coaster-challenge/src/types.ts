export enum Direction {
    NORTH = 'N',
    EAST = 'E',
    SOUTH = 'S',
    WEST = 'W',
}

export type Track = {
    trackPieces: TrackPiece[],
    posts: Post[],
    tunnels?: Tunnel[],
    xLength: number,
    yLength: number,
};

export type TrackPiece = Record<string, any> & Position;

export type Post = Position;

export type Tunnel = {
    orientation: 'vertical' | 'horizontal';
    x: number;
    y: number;
    maxHeight: number;
};

export type Position = {
    x: number,
    y: number,
    height: number,
};
