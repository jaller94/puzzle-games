import { TrackPiece } from "../types";

export const getCurves = (trackPieces) => (
    trackPieces.filter(({type}) => type === 'curve')
);

export const getEnd = (trackPieces: TrackPiece[]): TrackPiece | undefined => (
    trackPieces.find(({type}) => type === 'end')
);

export const getLoops = (trackPieces: TrackPiece[]) => (
    trackPieces.filter(({type}) => type === 'loop')
);

export const getStart = (trackPieces) => (
    trackPieces.find(({type}) => type === 'start')
);

export const getStraights = (trackPieces) => (
    trackPieces.find(({type}) => type === 'straight')
);
