export const inventory = {
    pieces: [
        {
            color: 'pink',
            data: '###\n  ##',
        },
        {
            color: 'blue',
            data: '###\n  #\n  #',
        },
        {
            color: 'lightblue',
            data: '#\n##',
        },
        {
            color: 'yellow',
            data: '####\n #',
        },
        {
            color: 'turquoise',
            data: '##\n##\n #',
        },
        {
            color: 'orange',
            data: '  #\n###\n #',
        },
        {
            color: 'red',
            data: '##\n #\n #\n #',
        },
        {
            color: 'purple',
            data: '##\n ##\n  #',
        },
        {
            color: 'darkblue',
            data: '##\n #\n #',
        },
        {
            color: 'green',
            data: '# #\n###',
        },
        {
            color: 'darkred',
            data: ' ##\n##',
        },
        {
            color: 'darkgreen',
            data: ' #\n###',
        },
    ]
};
