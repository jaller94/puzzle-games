export function createEmptyBoard(width: number = 11, height: number = 5): string {
    const row = ' '.repeat(width);
    const board = `${row}\n`.repeat(height);
    return board.slice(0, -1);
}

export function placePiece(board: string, pieceData: string, pieceId: string, x: number, y: number): string {
    throw Error('NOT_IMPLEMENTED');
}
