import {
    actors,
    board,
} from './inventory';

type Relation = Record<string, any>;

/**
 * Resolves a Relation attribute of a statement to a set of seats this could refer to.
 * Examples: The seat "opposite of" 4 is 2. The seats "next to" 0 are 1 and 5.
 * 
 * This function works with any even board size. The examples imply a board size of 6.
 */
function applyRelation(referenceSeatIndex: number, relation: Relation): Set<number> {
    if (referenceSeatIndex < 0) {
        throw Error(`referenceSeatIndex must be valid. got: ${referenceSeatIndex}`);
    }
    const seatIndexes: Set<number> = new Set();
    if (!relation) {
        seatIndexes.add(referenceSeatIndex);
    } else {
        if (relation.type === 'oppositeOf') {
            if (board.length !== 6) {
                throw Error('Boards must have 6 seats the relation type oppositeOf');
            }
            if (referenceSeatIndex === 1) {
                seatIndexes.add(5);
            } else if (referenceSeatIndex === 2) {
                seatIndexes.add(4);
            } else if (referenceSeatIndex === 4) {
                seatIndexes.add(2);
            } else if (referenceSeatIndex === 5) {
                seatIndexes.add(1);
            } else {
                seatIndexes.add((referenceSeatIndex + (board.length / 2) + board.length) % board.length);
            }
        }
        if (relation.type === 'directionalOffset') {
            seatIndexes.add((referenceSeatIndex - relation.value + board.length) % board.length);
        }
        if (relation.type === 'offset') {
            seatIndexes.add((referenceSeatIndex + relation.value + board.length) % board.length);
            seatIndexes.add((referenceSeatIndex - relation.value + board.length) % board.length);
        }
    }
    return seatIndexes;
}

/**
 * Returns a set of seatIds at which a certain item can be found.
 */
function findItems(itemId: string): Set<number> {
    const seatIds: Set<number> = new Set();
    for (let i = 0; i < board.length; i++) {
        const seat = board[i];
        if (seat.items.includes(itemId)) {
            seatIds.add(i);
        }
    }
    return seatIds;
}

function placeActor(seats, actor, seatIndex) {
    if (seats[seatIndex].actor !== undefined || seats[seatIndex].actor === actor) {
        throw new Error(`Trying to place an actor on a non-empty seat.`);
    }
    seats[seatIndex].actor = actor;
    seats[seatIndex].possibleActors.clear();
    seats.forEach(seat => seat.possibleActors.delete(actor));
}

export function getPossibleSeatsForActor(seats, actor) {
    const seatIds = new Set();
    for (let i = 0; i < seats.length; i++) {
        const seat = seats[i];
        if (seat.possibleActors.has(actor)) {
            seatIds.add(i);
        }
    }
    return seatIds;
}

export function solvePuzzle(statements, crime: string) {
    const seats: {
        actor: string | undefined,
        possibleActors: Set<string>,
    }[] = [];
    for (const seat of board) {
        seats.push({
            actor: undefined,
            possibleActors: new Set(Object.keys(actors)),
        });
    }
    for (let loopIndex = 0; loopIndex < seats.length; loopIndex++) {
        for (const statement of statements) {
            if (statement.fulfilled) continue;
            console.log(statement);
            if (statement.type === 'position') {
                if (statement.reference.type === 'position') {
                    const referenceSeatIndex = board.findIndex(seat => seat.crime === statement.reference.id);
                    let seatIndexes = applyRelation(referenceSeatIndex, statement.relation);
                    if (seatIndexes.size === 1) {
                        const seatIndex = [...seatIndexes.values()][0];
                        placeActor(seats, statement.who, seatIndex);
                        statement.fulfilled = true;
                    }
                }
                if (statement.reference.type === 'actor') {
                    const referenceSeatIndex = seats.findIndex(seat => seat.actor === statement.reference.id);
                    let seatIndexes = applyRelation(referenceSeatIndex, statement.relation);
                    for (const seatIndex of seatIndexes) {
                        if (seats[seatIndex].actor !== undefined) {
                            seatIndexes.delete(seatIndex);
                        }
                    }
                    if (seatIndexes.size === 1) {
                        const seatIndex = [...seatIndexes.values()][0];
                        placeActor(seats, statement.who, seatIndex);
                        statement.fulfilled = true;
                    }
                }
                if (statement.reference.type === 'item') {
                    const referenceSeatIndexes = findItems(statement.reference.id);
                    for (const referenceSeatIndex of referenceSeatIndexes) {
                        if (!seats[referenceSeatIndex].possibleActors.has(statement.who)) {
                            continue;
                        }
                        let seatIndexes = applyRelation(referenceSeatIndex, statement.relation);
                        for (const seatIndex of seatIndexes) {
                            if (seats[seatIndex].actor !== undefined) {
                                seatIndexes.delete(seatIndex);
                            }
                        }
                        if (seatIndexes.size === 1) {
                            const seatIndex = [...seatIndexes.values()][0];
                            placeActor(seats, statement.who, seatIndex);
                            statement.fulfilled = true;
                            break;
                        }
                        throw Error('Could not place actor');
                    }
                }
            } else if (statement.type === 'absent') {
                const actors = Array.isArray(statement.who) ? statement.who : [statement.who];
                for (const actor of actors) {
                    seats.forEach(seat => seat.possibleActors.delete(actor));
                }
                //FIXME At the end, fill empty seats with null.
                // seats.forEach(seat => seat.possibleActors.add(null));
                statement.fulfilled = true;
            }
            //TODO In the worst case this must be run as often as we have seats.
            for (let i = 0; i < seats.length; i++) {
                const seat = seats[i];
                if (seat.actor === undefined && seat.possibleActors.size === 1) {
                    const actor = [...seat.possibleActors.values()][0];
                    // Can the dog only be placed here in case of actors being absent?
                    if (getPossibleSeatsForActor(seats, actor).size === 1) {
                        placeActor(seats, actor, i);
                    }
                }
                if (seat.actor === undefined && seat.possibleActors.size === 0) {
                    placeActor(seats, null, i);
                }
            }
        }
    }
    const crimeSeatIndex = board.findIndex(seat => seat.crime === crime);
    return {
        debug: seats,
        seats: seats.map(seat => seat.actor),
        culprit: seats[crimeSeatIndex].actor,
    }
}

export function getActorName(id: string): string {
    return actors[id]['name'];
}

export function referenceToString(reference): string {
    if (reference.type === 'and') {
        return `${referenceToString(reference.references[0])} und ${referenceToString(reference.references[1])}`;
    } else if (reference.type === 'or') {
        return `${referenceToString(reference.references[0])} oder ${referenceToString(reference.references[1])}`;
    } else if (reference.type === 'position') {
        if (reference.id === 'homework') {
            return 'den Hausaufgaben';
        } else if (reference.id === 'cake') {
            return 'der Torte';
        } else if (reference.id === 'shoes') {
            return 'den Schuhen';
        } else if (reference.id === 'cussion') {
            return 'dem Kissen';
        } else if (reference.id === 'poop') {
            return 'der Kacke';
        } else if (reference.id === 'planter') {
            return 'dem Blumentopf';
        }
    } else if (reference.type === 'item') {
        if (reference.id === 'bone') {
            return 'einem Kauknochen';
        } else if (reference.id === 'tennis-ball') {
            return 'einem Tennisball';
        } else if (reference.id === 'stick') {
            return 'einem Stock';
        } else if (reference.id === 'rope') {
            return 'einem Spielseil';
        } else if (reference.id === 'paw-print') {
            return 'einem Pfotenabdruck';
        } else if (reference.id === 'sock') {
            return 'einer Socke';
        }
    } else if (reference.type === 'attribute') {
        if (reference.id === 'ribbon') {
            return 'einem Hund mit einee Schleife';
        } else if (reference.id === 'white-paws') {
            return 'einem Hund mit weißen Pfoten';
        } else if (reference.id === 'white-paws') {
            return 'einem Hund mit weißen Pfoten';
        } else if (reference.id === 'collar') {
            return 'einem Hund mit einem Halsband';
        } else if (reference.id === 'scarf') {
            return 'einem Hund mit einem Halstuch';
        } else if (reference.id === 'light-brown-tail') {
            return 'einem Hund mit einem hellbraunen Schwanz';
        }
    } else if (reference.type === 'actor') {
        return getActorName(reference.id);
    } else if (reference.type === 'unknownActor') {
        return 'einem Hund';
    }
    throw Error('Unknown reference');
}

export function toString(statement): string {
    if (statement.type === 'absent') {
        if (typeof statement.who === 'string') {
            return `${getActorName(statement.who)} hat im Hof gespielt.`;
        } else if (Array.isArray(statement.who)) {
            //FIXME More than two names should get separated by commas.
            const actors = statement.who.map(getActorName).join(' und ');
            return `${actors} haben im Hof gespielt.`;
        } else if (statement.who.type === 'actorCount') {
            if (statement.who.value === 1) {
                return 'Ein Hund hat im Hof gespielt.';
            } else {
                const amount = [
                    , , 'Zwei', 'Drei', 'Vier', 'Fünf',
                ][statement.who.value];
                return `${amount} Hunde haben im Hof gespielt.`;
            }
        }
    } else if (statement.type === 'position') {
        let actor;
        let relation;
        if (statement.who === null) {
            actor = 'Niemand'; // On some cards also "Kein Hund"
        } else if (typeof statement.who === 'string') {
            actor = getActorName(statement.who);
        } else if (statement.who.type === 'attribute') {
            if (statement.who.id === 'ribbon') {
                actor = 'Ein Hund mit einer Schleife';
            } else if (statement.who.id === 'white-paws') {
                actor = 'Ein Hund mit weißen Pfoten';
            } else if (statement.who.id === 'collar') {
                actor = 'Ein Hund mit einem Halsband';
            } else if (statement.who.id === 'scarf') {
                actor = 'Ein Hund mit einem Halstuch';
            } else if (statement.who.id === 'light-brown-tail') {
                actor = 'Ein Hund mit einem hellbraunen Schwanz';
            }
        }
        if (statement.relation) {
            if (statement.relation.type === 'offset') {
                if (statement.relation.value === 1) {
                    relation = 'neben';
                } else if (statement.relation.value === 2) {
                    relation = '2 Plätze entfernt von';
                } else if (statement.relation.value === 3) {
                    relation = '3 Plätze entfernt von';
                }
            } else if (statement.relation.type === 'directionalOffset') {
                if (statement.relation.value === 1) {
                    relation = 'rechts neben';
                } else if (statement.relation.value === -1) {
                    relation = 'links neben';
                }
            } else if (statement.relation.type === 'oppositeOf') {
                relation = 'gegenüber von';
            }
        } else {
            relation = 'vor';
        }
        const reference = referenceToString(statement.reference);
        return `${actor} saß${statement.not ? ' nicht' : ''} ${relation} ${reference}.`;
    }
    throw Error(`Unknown statement type: ${statement.type}`);
}
