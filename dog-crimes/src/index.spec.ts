import {
    solvePuzzle,
    toString,
} from './index';

describe('toString', () => {
    test('Puzzle 1', () => {
        expect(toString({
            who: 'poldi',
            type: 'position',
            reference: {
                type: 'position',
                id: 'homework',
            },
        })).toBe('Poldi saß vor den Hausaufgaben.');
        expect(toString({
            who: 'caesar',
            type: 'position',
            relation: {
                type: 'oppositeOf',
            },
            reference: {
                type: 'actor',
                id: 'poldi',
            },
        })).toBe('Cäsar saß gegenüber von Poldi.');
        expect(toString({
            who: 'daisy',
            type: 'position',
            relation: {
                type: 'directionalOffset',
                value: 1,
            },
            reference: {
                type: 'actor',
                id: 'caesar',
            },
        })).toBe('Daisy saß rechts neben Cäsar.');
        expect(toString({
            who: 'lucky',
            type: 'position',
            reference: {
                type: 'item',
                id: 'bone',
            },
        })).toBe('Lucky saß vor einem Kauknochen.');
        expect(toString({
            who: 'bella',
            type: 'position',
            relation: {
                type: 'offset',
                value: 1,
            },
            reference: {
                type: 'actor',
                id: 'lucky',
            },
        })).toBe('Bella saß neben Lucky.');
    });
    test('Puzzle 2', () => {
        expect(toString({
            who: [
                'daisy',
                'poldi',   
            ],
            type: 'absent',
        })).toBe('Daisy und Poldi haben im Hof gespielt.');
        expect(toString({
            who: 'bella',
            type: 'position',
            reference: {
                type: 'position',
                id: 'cake',
            },
        })).toBe('Bella saß vor der Torte.');
    });
    test('Puzzle 3 A', () => {
        expect(toString({
            who: {
                type: 'actorCount',
                value: 3,
            },
            type: 'absent',
        })).toBe('Drei Hunde haben im Hof gespielt.');
    });
    test('Puzzle 4 A', () => {
        expect(toString({
            who: 'trixi',
            type: 'position',
            reference: {
                type: 'and',
                references: [
                    {
                        type: 'item',
                        id: 'bone',
                    }, {
                        type: 'item',
                        id: 'stick',
                    },
                ],
            },
        })).toBe('Trixi saß vor einem Kauknochen und einem Stock.');
    });
    test('Puzzle 4 E', () => {
        expect(toString({
            who: 'poldi',
            type: 'position',
            relation: {
                type: 'offset',
                value: 1,
            },
            reference: {
                type: 'attribute',
                id: 'scarf',
            },
        })).toBe('Poldi saß neben einem Hund mit einem Halstuch.');
    });
    test('Puzzle 5', () => {
        expect(toString({
            who: {
                type: 'attribute',
                id: 'white-paws',
            },
            type: 'position',
            relation: {
                type: 'oppositeOf',
            },
            reference: {
                type: 'actor',
                id: 'bella',
            },
        })).toBe('Ein Hund mit weißen Pfoten saß gegenüber von Bella.');
        expect(toString({
            who: {
                type: 'attribute',
                id: 'ribbon',
            },
            type: 'position',
            relation: {
                type: 'oppositeOf',
            },
            reference: {
                type: 'actor',
                id: 'trixi',
            },
        })).toBe('Ein Hund mit einer Schleife saß gegenüber von Trixi.');
    });
    test('Puzzle 13', () => {
        expect(toString({
            who: 'trixi',
            type: 'absent',
        })).toBe('Trixi hat im Hof gespielt.');
        expect(toString({
            who: 'lucky',
            type: 'position',
            reference: {
                type: 'position',
                id: 'shoes',
            },
        })).toBe('Lucky saß vor den Schuhen.');
        expect(toString({
            who: {
                type: 'attribute',
                id: 'ribbon',
            },
            type: 'position',
            relation: {
                type: 'offset',
                value: 3,
            },
            reference: {
                type: 'actor',
                id: 'lucky',
            },
        })).toBe('Ein Hund mit einer Schleife saß 3 Plätze entfernt von Lucky.');
        expect(toString({
            who: null,
            type: 'position',
            relation: {
                type: 'oppositeOf',
            },
            reference: {
                type: 'actor',
                id: 'bella',
            },
        })).toBe('Niemand saß gegenüber von Bella.');
        expect(toString({
            who: 'daisy',
            type: 'position',
            not: true,
            reference: {
                type: 'item',
                id: 'paw-print',
            },
        })).toBe('Daisy saß nicht vor einem Pfotenabdruck.');
        expect(toString({
            who: 'poldi',
            type: 'position',
            reference: {
                type: 'item',
                id: 'tennis-ball',
            },
        })).toBe('Poldi saß vor einem Tennisball.');
    });
    test('Puzzle 28 F', () => {
        expect(toString({
            who: 'bella',
            type: 'position',
            not: true,
            reference: {
                type: 'or',
                references: [
                    {
                        type: 'item',
                        id: 'bone',
                    }, {
                        type: 'item',
                        id: 'sock',
                    },
                ],
            },
        })).toBe('Bella saß nicht vor einem Kauknochen oder einer Socke.');
    });
    test('Puzzle 36', () => {
        expect(toString({
            who: 'lucky',
            type: 'position',
            relation: {
                type: 'offset',
                value: 2,
            },
            reference: {
                type: 'actor',
                id: 'caesar',
            },
        })).toBe('Lucky saß 2 Plätze entfernt von Cäsar.');
        expect(toString({
            who: 'poldi',
            type: 'position',
            relation: {
                type: 'offset',
                value: 3,
            },
            reference: {
                type: 'actor',
                id: 'bella',
            },
        })).toBe('Poldi saß 3 Plätze entfernt von Bella.');
        expect(toString({
            who: {
                type: 'attribute',
                id: 'white-paws',   
            },
            type: 'position',
            not: true,
            reference: {
                type: 'item',
                id: 'sock',
            },
        })).toBe('Ein Hund mit weißen Pfoten saß nicht vor einer Socke.');
    });
    test('Puzzle ?', () => {
        expect(toString({
            who: 'daisy',
            type: 'position',
            not: true,
            relation: {
                type: 'oppositeOf',
            },
            reference: {
                type: 'unknownActor',
            },
        })).toBe('Daisy saß nicht gegenüber von einem Hund.');
    });
    test('Puzzle ?', () => {
        expect(toString({
            who: null,
            type: 'position',
            relation: {
                type: 'offset',
                value: 1,
            },
            reference: {
                type: 'unknownActor',
            },
        })).toBe('Keine Hunde saßen nebeneinander.');
    });
});

describe('solvePuzzle', () => {
    test('Puzzle 1', () => {
        const statements = [
            {
                who: 'poldi',
                type: 'position',
                reference: {
                    type: 'position',
                    id: 'homework',
                },
            },
            {
                who: 'caesar',
                type: 'position',
                relation: {
                    type: 'oppositeOf',
                },
                reference: {
                    type: 'actor',
                    id: 'poldi',
                },
            },
            {
                who: 'daisy',
                type: 'position',
                relation: {
                    type: 'directionalOffset',
                    value: 1,
                },
                reference: {
                    type: 'actor',
                    id: 'caesar',
                },
            },
            {
                who: 'lucky',
                type: 'position',
                reference: {
                    type: 'item',
                    id: 'bone',
                },
            },
            {
                who: 'bella',
                type: 'position',
                relation: {
                    type: 'offset',
                    value: 1,
                },
                reference: {
                    type: 'actor',
                    id: 'lucky',
                },
            },
        ];
        const actual = solvePuzzle(statements, 'cussion');
        expect(actual.seats).toEqual([
            'caesar',
            'bella',
            'lucky',
            'poldi',
            'trixi',
            'daisy',
        ]);
        expect(actual.culprit).toBe('trixi');
    });
    test('Puzzle 2', () => {
        const statements = [
            {
                who: [
                    'daisy',
                    'poldi',   
                ],
                type: 'absent',
            },
            {
                who: 'bella',
                type: 'position',
                reference: {
                    type: 'position',
                    id: 'cake',
                },
            },
            {
                who: 'trixi',
                type: 'position',
                relation: {
                    type: 'directionalOffset',
                    value: -1,
                },
                reference: {
                    type: 'actor',
                    id: 'bella',
                },
            },
            {
                who: 'lucky',
                type: 'position',
                relation: {
                    type: 'offset',
                    value: 2,
                },
                reference: {
                    type: 'actor',
                    id: 'bella',
                },
            },
            {
                who: 'caesar',
                type: 'position',
                relation: {
                    type: 'offset',
                    value: 1,
                },
                reference: {
                    type: 'actor',
                    id: 'trixi',
                },
            },
        ];
        const actual = solvePuzzle(statements, 'cussion');
        expect(actual.seats).toEqual([
            'bella',
            'trixi',
            'caesar',
            null,
            'lucky',
            null,
        ]);
        expect(actual.culprit).toBe('lucky');
    });
    test('Puzzle 3', () => {
        const statements = [
            {
                who: {
                    type: 'actorCount',
                    value: 3,
                },
                type: 'absent',
            },
            {
                who: 'poldi',
                type: 'position',
                reference: {
                    type: 'item',
                    id: 'tennis-ball',
                },
            },
            {
                who: 'lucky',
                type: 'position',
                not: true,
                relation: {
                    type: 'offset',
                    value: 1,
                },
                reference: {
                    type: 'unknownActor',
                },
            },
            {
                who: [
                    'trixie',
                    'lucky',
                ],
                type: 'position',
                reference: {
                    type: 'item',
                    id: 'stick',
                },
            },
        ];
        const actual = solvePuzzle(statements, 'cussion');
        expect(actual.seats).toEqual([
            'lucky',
            null,
            'trixie',
            'poldi',
            null,
            null,
        ]);
        expect(actual.culprit).toBe('poldi');
    });
    test('Puzzle 4', () => {
        const statements = [
            {
                who: 'trixi',
                type: 'position',
                reference: {
                    type: 'and',
                    references: [
                        {
                            type: 'item',
                            id: 'bone',
                        }, {
                            type: 'item',
                            id: 'stick',
                        },
                    ],
                },
            },
            // TODO More statements
        ];
        const actual = solvePuzzle(statements, 'poop');
        expect(actual.seats).toEqual([
            'daisy',
            'caesar',
            'poldi',
            'lucky',
            'bella',
            'trixie',
        ]);
        expect(actual.culprit).toBe('bella');
    });
    test('Puzzle 13', () => {
        const statements = [
            {
                who: 'trixi',
                type: 'absent',
            },
            {
                who: 'lucky',
                type: 'position',
                reference: {
                    type: 'position',
                    id: 'shoes',
                },
            },
            {
                who: {
                    type: 'attribute',
                    id: 'ribbon',
                },
                type: 'position',
                relation: {
                    type: 'offset',
                    value: 3,
                },
                reference: {
                    type: 'actor',
                    id: 'lucky',
                },
            },
            {
                who: null,
                type: 'position',
                relation: {
                    type: 'oppositeOf',
                },
                reference: {
                    type: 'actor',
                    id: 'bella',
                },
            },
            {
                who: 'daisy',
                type: 'position',
                not: true,
                reference: {
                    type: 'item',
                    id: 'paw-print',
                },
            },
            {
                who: 'poldi',
                type: 'position',
                reference: {
                    type: 'item',
                    id: 'tennis-ball',
                },
            },
        ];
        const actual = solvePuzzle(statements, 'cussion');
        expect(actual.seats).toEqual([
        ]);
        expect(actual.culprit).toBe();
    });
});
