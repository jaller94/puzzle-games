export const actors = {
    bella: {
        name: 'Bella',
        attributes: [
            'collar',
            'ribbon',
        ],
    },
    caesar: {
        name: 'Cäsar',
        attributes: [
            'scarf',
            'pointed-ears'
        ],
    },
    daisy: {
        name: 'Daisy',
        attributes: [
            'scarf',
            'light-brown-tail',
        ],
    },
    lucky: {
        name: 'Lucky',
        attributes: [
            'white-paws',
            'collar',
        ],
    },
    poldi: {
        name: 'Poldi',
        attributes: [
            'light-brown-tail',
        ],
    },
    trixi: {
        name: 'Trixi',
        attributes: [
            'white-paws',
            'pointed-ears',
        ],
    },
};

export const board = [
    {
        crime: 'cake',
        items: [
            'rope',
            'stick',
        ],
    },
    {
        crime: 'poop',
        items: [
            'tennis-ball',
            'paw-print',
        ],
    },
    {
        crime: 'shoes',
        items: [
            'stick',
            'bone',
        ],
    },
    {
        crime: 'homework',
        items: [
            'sock',
            'tennis-ball',
        ],
    },
    {
        crime: 'cussion',
        items: [
            'rope',
            'paw-print',
        ],
    },
    {
        crime: 'planter',
        items: [
            'bone',
            'sock',
        ],
    },
];
