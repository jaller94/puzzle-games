import * as fs from 'node:fs';
import Ajv from 'ajv/dist/2020';
const ajv = new Ajv({allErrors: true});

const schema = JSON.parse(fs.readFileSync('./iq-twist/schemas/puzzle/2022-09.json', 'utf8'));
const puzzle1 = JSON.parse(fs.readFileSync('./iq-twist/official-puzzles/1.json', 'utf8'));

describe('track schema', () => {
    test('validates 1.json', () => {
        const validate = ajv.compile(schema);
        validate(puzzle1);
        expect(validate.errors).toBe(null);
    });
});
