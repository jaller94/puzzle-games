const field = document.getElementById('field');

const data = {
    $schema: 'https://chrpaul.de/schemas/tip-over/2022-07/schema.json',
    id: undefined,
    jumperPosition: undefined,
    pieces: [],
};

const heightOrder = [undefined, 2, 3, 4, 1];

let placingJumper = false;

document.getElementById('btn-placing-jumper').addEventListener('click', (event) => {
    event.preventDefault();
    event.stopPropagation();
    placingJumper = !placingJumper;
});

document.getElementById('input-id').addEventListener('input', (event) => {
    event.preventDefault();
    event.stopPropagation();
    data.id = event.target.value || undefined;
    outputTrack();
});

function handleClick(event) {
    event.preventDefault();
    event.stopPropagation();
    const position = Number.parseInt(event.target.dataset['no']);

    if (placingJumper) {
        event.target.classList.add(`jumper`);
        placingJumper = false;
        data.jumperPosition = position;
        return;
    } else {
        let current = data.pieces.find(piece => position === piece.position);
        if (!current) {
            current = {
                position,
                height: heightOrder[0],
            };
            data.pieces = [
                ...data.pieces,
                current,
            ];
            data.pieces.sort((a, b) => a.position - b.position);
        }
        if (current.height >= 1) {
            event.target.classList.remove(`height${current.height}`);
        }
        current.height = heightOrder[heightOrder.findIndex(e => e === current.height) + 1 % heightOrder.length];
        if (current.height <= 4) {
            event.target.classList.add(`height${current.height}`);
        }
        if (current.height === undefined) {
            data.pieces = data.pieces.filter(piece => position !== piece.position);
        }
    }
    outputTrack();
}

for (let index = 1; index <= 6*6; index++) {
    const element = document.createElement('div');
    element.dataset['no'] = index;
    element.addEventListener('click', handleClick);
    field.appendChild(element);
}

function outputTrack() {
    const output = document.getElementById('output');
    output.value = JSON.stringify(data, null, 2);
}
