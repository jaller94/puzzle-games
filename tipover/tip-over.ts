export function check(root: any) {
    const errors: string[] = [];
    if (typeof root !== 'object') {
        errors.push('root must be an object');
        return errors;
    }
    if (!Number.isInteger(root.jumperPosition)) {
        errors.push('"jumperPosition" must be an integer');
    } else if (root.jumperPosition < 1 || root.jumperPosition > 36) {
        errors.push('"pieces" must be [1 - 36]');
    }
    if (!Array.isArray(root.pieces)) {
        errors.push('"pieces" must be an array');
    } else if (root.pieces.length < 1 || root.pieces.length > 36) {
        errors.push('"pieces" must have [1 - 36] items');
    } else {
        const occupiedPositions = new Set();
        for (let i = 0; i < root.pieces.length; i++) {
            const piece = root.pieces[i];
            if (typeof piece !== 'object') {
                errors.push(`"piece[${i}]" must be an object`);
            }
            if (!Number.isInteger(piece.position)) {
                errors.push(`"piece[${i}].position" must be an integer`);
                continue;
            } else if (piece.position < 1 || piece.position > 36) {
                errors.push(`"piece[${i}].position" must be [1 - 36]`);
                continue;
            }
            if (occupiedPositions.has(piece.position)) {
                errors.push(`"piece[${i}].position" is a duplicate`);
            }
            occupiedPositions.add(piece.position);
        }
        if (root.pieces.findIndex(p => p.height === 1) === -1) {
            errors.push('"pieces" must have an item of height 1 (the goal)');
        }
    }
    return errors;
}

function isWalkable(heights, position) {
    if (position < 1 && position > 36) throw Error(`AAAAAHHHH ${position}`);
    return heights[position - 1] > 0;
}

export function getWalkablePositions(heights, jumperPosition: number) {
    const walkablePositions: Set<number> = new Set();
    walkablePositions.add(jumperPosition);
    for (const position of walkablePositions) {
        if (position > 6 && isWalkable(heights, position - 6)) {
            walkablePositions.add(position - 6);
        }
        if (position % 6 !== 1 && isWalkable(heights, position - 1)) {
            walkablePositions.add(position - 1);
        }
        if (position % 6 !== 0 && isWalkable(heights, position + 1)) {
            walkablePositions.add(position + 1);
        }
        if (position < 31 && isWalkable(heights, position + 6)) {
            walkablePositions.add(position + 6);
        }
    }
    return walkablePositions;
}

export function tip(heights, position, x: number, y: number) {
    const height = heights[position - 1];
    const topPosition = position + height * x + height * y * 6;
    const positionRow = Math.floor((position - 1) / 6);
    const topPositionRow = Math.floor((topPosition - 1) / 6);
    if (topPosition < 1 || topPosition > 36 || (y === 0 && positionRow !== topPositionRow)) {
        throw Error('Crate falls outside the grid');
    }
    const newHeights = Array.from(heights);
    newHeights[position - 1] = undefined;
    for (let i = 1; i <= height; i++) {
        const targetPosition = position + i * x + i * y * 6;
        if (newHeights[targetPosition - 1] !== undefined) {
            throw Error(`Crate falls on another crate at ${targetPosition}`);
        }
        newHeights[targetPosition - 1] = 1;
    }
    if (newHeights.length !== 36) {
        throw Error('NOT 36');
    }
    return {
        heights: newHeights,
        jumperPosition: topPosition,
    };
}

function tipWrapper(heights, jumperPosition: number, goalPosition: number, instructions, x: number, y: number, direction) {
    let tipped;
    try {
        tipped = tip(heights, jumperPosition, x, y);
    } catch (error) {
        if (error.message.startsWith('Crate falls ')) {
            return;
        }
        throw error;
    }
    const subInstructions = searchSolutions(tipped.heights, tipped.jumperPosition, goalPosition);
    // if (jumperPosition === 6 && direction === 'D') {
    //     let input = tipped.heights.map(a => a ?? 0).join('');
    //     let str = '';
    //     for (let i = 0; i < input.length; i++) {
    //         str += input[i];
    //         if (i % 6 === 5) {
    //             str += '\n';
    //         }
    //     }
    //     console.log(str, tipped.jumperPosition, goalPosition);
    //     console.dir(subInstructions);
    //     console.dir(getWalkablePositions(tipped.heights, tipped.jumperPosition));
    // }
    if (subInstructions === undefined) {
        return;
    }
    if (subInstructions.length === 0) {
        instructions.push([
            {
                direction,
                position: jumperPosition,
            }
        ]);
    }
    for (const subInstruction of subInstructions) {
        instructions.push([
            {
                direction,
                position: jumperPosition,
            },
            ...subInstruction,
        ]);
    }
}

function searchSolutions(heights, jumperPosition, goalPosition) {
    let instructions = [];
    const walkablePositions = getWalkablePositions(heights, jumperPosition);
    if (walkablePositions.has(goalPosition)) {
        return [];
    }
    for (const position of walkablePositions) {
        if (heights[position - 1] > 1) {
            tipWrapper(heights, position, goalPosition, instructions, 0, -1, 'U');
            tipWrapper(heights, position, goalPosition, instructions, 1, 0, 'R');
            tipWrapper(heights, position, goalPosition, instructions, 0, 1, 'D');
            tipWrapper(heights, position, goalPosition, instructions, -1, 0, 'L');
        }
    }
    if (instructions.length > 0) {
        return instructions;
    }
}

export function solve(root) {
    const heights = Array(36);
    let goal = root.pieces.find(p => p.height === 1).position;
    for (const piece of root.pieces) {
        heights[piece.position - 1] = piece.height;
    }
    return searchSolutions(heights, root.jumperPosition, goal);
}
