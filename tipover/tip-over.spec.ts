import { tip, getWalkablePositions } from './tip-over';

describe('tip', () => {
    test('tips a crate of height 1 to the top', () => {
        const heights = new Array(36);
        heights[6] = 1;
        const tipped = tip(heights, 7, 0, -1);
        heights[6] = undefined;
        heights[0] = 1;
        expect(tipped.jumperPosition).toBe(1);
        expect(tipped.heights).toEqual(heights);
    });
    test('tips a crate of height 2 to the right', () => {
        const heights = new Array(36);
        heights[6] = 2;
        const tipped = tip(heights, 7, 1, 0);
        heights[6] = undefined;
        heights[7] = 1;
        heights[8] = 1;
        expect(tipped.jumperPosition).toBe(9);
        expect(tipped.heights).toEqual(heights);
    });
    test('prevents tipping into another crate', () => {
        const heights = new Array(36);
        heights[24] = 3;
        heights[27] = 3;
        const func = () => tip(heights, 25, 1, 0);
        expect(func).toThrow('Crate falls on another crate at 28');
    });
    test('prevents tipping over the grid at the top', () => {
        const heights = new Array(36);
        heights[0] = 1;
        const func = () => tip(heights, 1, 0, -1);
        expect(func).toThrow('Crate falls outside the grid');
    });
    test('prevents tipping over the grid at the bottom', () => {
        const heights = new Array(36);
        heights[31] = 1;
        const func = () => tip(heights, 32, 0, 1);
        expect(func).toThrow('Crate falls outside the grid');
    });
    test('prevents tipping over the grid to the right', () => {
        const heights = new Array(36);
        heights[5] = 1;
        const func = () => tip(heights, 6, 1, 0);
        expect(func).toThrow('Crate falls outside the grid');
    });
    test('prevents tipping over the grid at the left', () => {
        const heights = new Array(36);
        heights[6] = 1;
        const func = () => tip(heights, 7, -1, 0);
        expect(func).toThrow('Crate falls outside the grid');
    });
});

describe('getWalkablePositions', () => {
    test('knows to walk to all sides at position 8 (1, 1)', () => {
        const heights = new Array(36);
        heights[1] = 1;
        heights[6] = 1;
        heights[7] = 1;
        heights[8] = 1;
        heights[13] = 1;
        const actual = getWalkablePositions(heights, 8);
        expect(actual).toEqual(new Set([2,7,8,9,14]));
    });
    test('knows to walk to all sides at position 9 (2, 1)', () => {
        const heights = new Array(36);
        heights[2] = 1;
        heights[7] = 1;
        heights[8] = 1;
        heights[9] = 1;
        heights[14] = 1;
        const actual = getWalkablePositions(heights, 9);
        expect(actual).toEqual(new Set([3,8,9,10,15]));
    });
    test('knows to walk to all sides at position 10 (3, 1)', () => {
        const heights = new Array(36);
        heights[3] = 1;
        heights[8] = 1;
        heights[9] = 1;
        heights[10] = 1;
        heights[15] = 1;
        const actual = getWalkablePositions(heights, 10);
        expect(actual).toEqual(new Set([4,9,10,11,16]));
    });
    test('knows to walk to all sides at position 11 (4, 1)', () => {
        const heights = new Array(36);
        heights[4] = 1;
        heights[9] = 1;
        heights[10] = 1;
        heights[11] = 1;
        heights[16] = 1;
        const actual = getWalkablePositions(heights, 11);
        expect(actual).toEqual(new Set([5,10,11,12,17]));
    });
    test('does not walk wrap to the right at 6 (5, 0)', () => {
        const heights = new Array(36);
        heights[5] = 1;
        heights[6] = 1;
        const actual = getWalkablePositions(heights, 6);
        expect(actual).toEqual(new Set([6]));
    });
    test('does not walk wrap to the left at 7 (0, 1)', () => {
        const heights = new Array(36);
        heights[5] = 1;
        heights[6] = 1;
        const actual = getWalkablePositions(heights, 7);
        expect(actual).toEqual(new Set([7]));
    });
});