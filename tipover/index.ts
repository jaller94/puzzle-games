import fsPromises from 'node:fs/promises';
import { check, solve } from './tip-over';

async function main() {
    const files = await fsPromises.readdir('official');
    for (const file of files) {
        // if (file !== '5.json') continue;
        const content = await fsPromises.readFile(`official/${file}`, 'utf8');
        const root = JSON.parse(content);
        const errors = await check(root);
        if (errors.length) {
            console.log(file);
            console.dir(errors);
        } else {
            const solutions = solve(root);
            if (!solutions) {
                console.warn(file, 'NO SOLUTIONS');
            } else {
                console.log(file, solutions.length > 1 ? `${solutions.length} solutions` : solutions[0].map(step => `${step.position}${step.direction}`).join(', '));
                // console.log(file, `${solutions.length} solutions`);
                // solutions.sort((a,b) => a.length - b.length);
                // if (solutions.length < 3) {
                //     for (const solution of solutions) {
                //         console.log(solution.map(step => `${step.position}${step.direction}`).join(', '));
                //     }
                // }
            }
        }
        // console.log();
    }
}

main();
