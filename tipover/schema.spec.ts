import * as fs from 'node:fs';
import Ajv from 'ajv';
const ajv = new Ajv({allErrors: true});

const schema = JSON.parse(fs.readFileSync('./tipover/schemas/draft-01.json', 'utf8'));
const puzzle1 = JSON.parse(fs.readFileSync('./tipover/official-puzzles/1.json', 'utf8'));
const puzzle2 = JSON.parse(fs.readFileSync('./tipover/official-puzzles/2.json', 'utf8'));

describe('track schema', () => {
  test('validates 1.json', () => {
    const validate = ajv.compile(schema);
    validate(puzzle1);
    expect(validate.errors).toBe(null);
  });
  test('validates 2.json', () => {
    const validate = ajv.compile(schema);
    validate(puzzle2);
    expect(validate.errors).toBe(null);
  });
});
